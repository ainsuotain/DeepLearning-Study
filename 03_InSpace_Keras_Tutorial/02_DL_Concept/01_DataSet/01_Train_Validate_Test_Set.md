* 1\. What is Learning the knowledge
    * 1.1\. Learn is making model to to understand the real.
    * 1.2\. For use in real after learn.
    * 1.3\. Learning has several step and process.

* 2\. Learn = Train <-> Test 

* 3\. Learn = Train, Validate, Test
    * 3.1\. Train 
    * 3.2\. Validate
        * What to validate = trained knowledge
        * How to validate  = updating the model by repeat each train-set and check with another-set.
        * Find out early stopping between underfitting and overfitting. (ex Frog in the wall)
        * cross validation is recommanded. (ex recusively pick up 1-set of 5-set for validate)
    * 3.3\. Test 
        * What to test = trained and validated knowledge
        * How to test  = Apply with the last updated model, check with another-set.
* 4\. Train, Validate, Test is relative.
