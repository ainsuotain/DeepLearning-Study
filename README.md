# Overview
| Component | Environment | Integrated Development Environment | Python |
|---|---|---|---|
| 01_Local_Tensorflow_Official_Docker | Local Container | Jupyter Notebook | 2.7  |
| 02_Local_PyCharm_Anaconda | Local macOS | IntelliJ PyCharm | 3.5 |

# 01_Local_Tensorflow_Official_Docker
| Contents | Details |
|---|---|
| Install | [by docker and jupyter](01_Local_Tensorflow_Official_Docker/01_Install/01_by_Docker.md) |
| Example | [1st : Multi Variable Linear Regression with manually](01_Local_Tensorflow_Official_Docker/02_Workspace/01_1st_Example/01_Multi_Variable_Linear_Regression_with_manually.ipynb) |
| | [2nd : Multi Variable Linear Regression with Matrix](01_Local_Tensorflow_Official_Docker/02_Workspace/02_2nd_Example/02_Multi_Variable_Linear_Regression_with_Matrix.ipynb) |
| | [3rd : Multi Variable Linear Regression with file](01_Local_Tensorflow_Official_Docker/02_Workspace/03_3rd_Example/03_Multi_Variable_Linear_Regression_with_file.ipynb) |
| | [4th : Multi Variable Linear Regression with fileQueue](01_Local_Tensorflow_Official_Docker/02_Workspace/04_4th_Example/04_Multi_Variable_Linear_Regression_with_fileQueue.ipynb) |
| | [5th : Multi Variable Linear Regression with Hattrick KP](01_Local_Tensorflow_Official_Docker/02_Workspace/05_5th_Example/05_Multi_Variable_Linear_Regression_with_file_Hattrick_KP.ipynb) |

# 02_SNU_DeepLearning_ReInforcement_Learning
| Contents | Details |
|---|---|
| Install | [Install PyCharm](02_SNU_ReInforcement_Learning/01_Install/01_Install_PyCharm.md) |
| | [Create VirEnv](02_SNU_ReInforcement_Learning/01_Install/02_Create_VirEnv.md) |
| | [Check Version of Packages](02_SNU_ReInforcement_Learning/01_Install/03_CheckVersionOfPackage.py) |
| Book | [Discount Factor](02_SNU_ReInforcement_Learning/02_RL_by_Python_and_Keras/ch02/01_MDP/05_Discount_Factor/01_Discount_Factor.md) |
| Example | |

# 03_InSpace_Keras_Tutorial
| Contents | Details |
|---|---|
| part 01 Start Keras | [Install by Anaconda](03_InSpace_Keras_Tutorial/01_Install_Keras/01_by_Anaconda.md) |
| | [Check Version of Packages](03_InSpace_Keras_Tutorial/01_Install_Keras/CheckVersionOfLibrary.ipynb) |
| part 02 DL Concept | [ch1 Data Set](03_InSpace_Keras_Tutorial/02_DL_Concept/01_DataSet/01_Train_Validate_Test_Set.md) |

# 04_DeepLearning_from_Scratch
| Contents | Details |
|---|---|
| ch1 Install Python | [Check Version of Python](04_DeepLearning_from_Scratch/1/1.3/python_version.ipynb) |
| ch2 Perceptron | [Perceptron : Definition, example for AND, NAND, OR, XOR](04_DeepLearning_from_Scratch/2/Perceptron.ipynb) |
| ch3 ANN | [From Perceptron to ANN : Step Function, Sigmoid Function](04_DeepLearning_from_Scratch/3/3.1/01_from_Perceptron_to_Artificial_Neural_Network.ipynb) |
